pipeline {
    agent any  // Runs on any available Jenkins agent
    
    stages {
        // Stage 1: Checkout the code
        stage('Checkout') {
            steps {
                // Checkout the source code from Git repository
                checkout scm  // Pulls the latest code from the repository
            }
        }
        
        // Stage 2: Install dependencies
        stage('Install Dependencies') {
            steps {
                echo 'Installing dependencies...'
                // Run npm install to install dependencies from package.json
                sh 'npm install'
            }
        }
        
        // Stage 3: Run Tests
        stage('Test') {
            steps {
                echo 'Running tests...'
                // Run tests using npm (assuming tests are in the test/ directory)
            }
        }
        
        // Stage 4: Build the Application
        stage('Build') {
            steps {
                echo 'Building the application...'
                // Build the application (typically, you would use a bundler like Webpack or other tools)
                sh 'npm run build'
            }
        }
        
        // Stage 5: Deploy to Staging
        stage('Deploy to Staging') {
            steps {
                echo 'Deploying to staging environment...'
                // Use Jenkins credentials to securely pass username and password to SCP
                sh 'cp -r dist /var/html/frontend'
            }
        }
    }
    
    post {
        success {
            echo 'Build succeeded! Sending success notification...'
            // Add steps for sending notifications (like Slack or email)
            // Example: Send success email, Slack notification, etc.
        }
        failure {
            echo 'Build failed! Sending failure notification...'
            // Add steps for sending failure notifications
            // Example: Send failure email, Slack notification, etc.
        }
    }
}

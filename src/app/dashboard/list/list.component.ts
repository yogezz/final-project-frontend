import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { CreateVoTs } from 'src/app/model/create-vo';
import { CreateService } from 'src/app/service/task.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent {
  filter = [
    'Task Number',
    'Task Description',
    'Assign',
    'Starting Date',
    'Estimate Date',
    'Task Status',
    'Sprint',
  ];
  userId = JSON.parse(localStorage.getItem('userId')!);
  filterForm: FormGroup;
  constructor(
    private taskService: CreateService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.filterForm = this.formBuilder.group({
      key: '',
      value: '',
    });
  }
  ngOnInit(): void {
    this.getTaskList();
    ``;
  }
  displayedColumns: string[] = [
    'taskNumber',
    'taskDescription',
    'assign',
    'startingDate',
    'estimateDate',
    'taskStatus',
    'sprint',
  ];
  dataSource!: MatTableDataSource<object>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  getTaskList() {
    if (localStorage.getItem('roleName') == 'Admin') {
      this.taskService.getTaskList().subscribe({
        next: (res: any) => {
          this.dataSource = new MatTableDataSource(res);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.backButton = false;
        },
        error: console.log,
      });
    } else {
      this.taskService.getAllTaskBUserId(this.userId).subscribe({
        next: (res: any) => {
          this.dataSource = new MatTableDataSource(res);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.backButton = false;
        },
        error: console.log,
      });
    }
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  camelCase(str: any) {
    let ans = str.toLowerCase();

    return ans
      .split(' ')
      .reduce(
        (s: any, c: string) => s + (c.charAt(0).toUpperCase() + c.slice(1))
      );
  }

  backButton!: boolean;

  submit() {
    this.taskService
      .getFilter(
        this.camelCase(this.filterForm.get('key')?.value),
        this.filterForm.get('value')?.value
      )
      .subscribe({
        next: (res: any) => {
          this.dataSource = new MatTableDataSource(res);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.backButton = true;
        },
        error: console.log,
      });
  }
  back() {
    this.getTaskList();
  }
}

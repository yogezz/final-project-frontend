import { Component } from '@angular/core';
import { LoginVo } from '../model/login-vo';
import { LoginService } from '../service/login.service';
import { Router, RouterLink } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  user: LoginVo = new LoginVo();

  constructor(
    private loginservice: LoginService,
    private router: Router,
    private toastr: ToastrService
  ) {}
  login() {
    console.log(this.user);
    this.loginservice.loginUser(this.user).subscribe(
      (data) => {
        localStorage.setItem('roleName', data.user.role[0].roleName);
        localStorage.setItem('accesstoken', data.jwtToken);
        localStorage.setItem('userId', data.user.userId);
        localStorage.setItem('userName', data.user.userName);
        this.toastr.success('Login Successfully');

        this.toastr.success('Welcome ' + localStorage.getItem('userName'));
        this.router.navigate(['/menu']);
      },
      (error) => {
        alert('Please enter valid username and password');
      }
    );
  }
}

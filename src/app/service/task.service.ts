import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateVoTs } from '../model/create-vo';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CreateService {
  private baseurl = 'http://localhost:8080/createTask';
  private baseurl1 = 'http://localhost:8080/getAllTask';
  private baseurl2 = 'http://localhost:8080/deleteTask/';
  private baseurl3 = 'http://localhost:8080/updateTask/';
  private baseurl4 = 'http://localhost:8080/getAllTaskByUserId/';
  private baseurl5 = 'http://localhost:8080/filterTask';
  constructor(private httpClient: HttpClient) {}
  createTask(task: CreateVoTs): Observable<Object> {
    console.log(task);
    return this.httpClient.post(this.baseurl, task);
  }

  updateTask(id: number, task: CreateVoTs): Observable<Object> {
    return this.httpClient.put(this.baseurl3 + id, task);
  }

  getTaskList(): Observable<object> {
    return this.httpClient.get(this.baseurl1);
  }

  deleteTask(id: number): Observable<Object> {
    return this.httpClient.delete(this.baseurl2 + id);
  }

  getAllTaskBUserId(id: number): Observable<Object> {
    return this.httpClient.get(this.baseurl4 + id);
  }

  getFilter(key: string, value: string): Observable<object> {
    const params = {
      key: key,
      value: value,
    };

    return this.httpClient.get(this.baseurl5, { params: params });
  }
}
